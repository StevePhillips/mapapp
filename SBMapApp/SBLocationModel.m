//
//  SBLocationModel.m
//  SBMapApp
//
//  Created by Student on 2015-03-25.
//  Copyright (c) 2015 Student. All rights reserved.
//

#import "SBLocationModel.h"

@implementation SBLocationModel

-(instancetype) initFromTable: (id) dic
{
    if(self = [super init])
    {
        if([dic isKindOfClass: [NSArray class]])
        {
            self.x = [[dic objectAtIndex:0] doubleValue];
            self.y = [[dic objectAtIndex:1] doubleValue];
            self.zoom = [[dic objectAtIndex:2] doubleValue];
        }
    }
    return self;
}

@end
