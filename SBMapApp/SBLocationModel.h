//
//  SBLocationModel.h
//  SBMapApp
//
//  Created by Student on 2015-03-25.
//  Copyright (c) 2015 Student. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBLocationModel : NSObject

@property unsigned long index;

@property double x;
@property double y;
@property double zoom;
@property NSString* name;

-(instancetype) initFromTable: (id) dic;

@end
