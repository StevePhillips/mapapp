//
//  ViewController.m
//  SBMapApp
//
//  Created by Student on 2015-03-18.
//  Copyright (c) 2015 Student. All rights reserved.
//

#import "SBGMapsViewController.h"
#import "SBEditPinViewController.h"
#include <GoogleMaps/GoogleMaps.h>


@interface SBGMapsViewController ()

@end

@implementation SBGMapsViewController
{
    GMSMapView* mapView_;
    GMSCameraPosition* camera;
    CLLocationCoordinate2D passed_coord;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //GMSCameraPosition* camera;
    
    // We will be viewing sydney if theres no location selected. Which can't happen from the user end.
    if(self.selected_location != nil)
    {
        if(camera == nil)
            camera = [GMSCameraPosition cameraWithLatitude:self.selected_location.x longitude:self.selected_location.y zoom:self.selected_location.zoom];
        else
            [mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:self.selected_location.x longitude:self.selected_location.y zoom:self.selected_location.zoom]];
    }
    else
    {
        camera = [GMSCameraPosition cameraWithLatitude:-33.86 longitude:151.20 zoom:6];
    }
    
    if(mapView_ == nil)
        mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    //else
        //[mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:self.selected_location.x longitude:self.selected_location.y zoom:self.selected_location.zoom]];
    
    mapView_.myLocationEnabled = YES;
    
    self.view = mapView_;
    mapView_.delegate = self;
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
}

- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    passed_coord = coordinate;
    [self performSegueWithIdentifier:@"showEditPinView" sender:self];
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"My segue");
    SBEditPinViewController* dest = segue.destinationViewController;
    
    dest.my_coord = passed_coord;
}

@end
