//
//  SBEditPinViewController.m
//  SBMapApp
//
//  Created by Angie B on 2015-04-05.
//  Copyright (c) 2015 Student. All rights reserved.
//

#import "SBEditPinViewController.h"

@interface SBEditPinViewController ()

@end

@implementation SBEditPinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.coordinate_outlet.text = [NSString stringWithFormat:@"Coordinates: %.3f X %.3f", self.my_coord.latitude, self.my_coord.longitude];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"Got coords: %.3f, %.3f", self.my_coord.latitude, self.my_coord.longitude);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)SaveButtonTouched:(id)sender {
}
@end
