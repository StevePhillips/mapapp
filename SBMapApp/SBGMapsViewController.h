//
//  ViewController.h
//  SBMapApp
//
//  Created by Student on 2015-03-18.
//  Copyright (c) 2015 Student. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "SBLocationModel.h"

@interface SBGMapsViewController : UIViewController <GMSMapViewDelegate>

@property SBLocationModel* selected_location;
@property BOOL transition;

@end

