//
//  SBLocationTableViewController.h
//  SBMapApp
//
//  Created by Student on 2015-03-25.
//  Copyright (c) 2015 Student. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBLocationTableViewController : UITableViewController<NSURLConnectionDelegate>

@end
