//
//  SBEditPinViewController.h
//  SBMapApp
//
//  Created by Angie B on 2015-04-05.
//  Copyright (c) 2015 Student. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface SBEditPinViewController : UIViewController

@property CLLocationCoordinate2D my_coord;

@property (weak, nonatomic) IBOutlet UILabel *coordinate_outlet;

@property (weak, nonatomic) IBOutlet UILabel *TapImageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *LocationImageView;



- (IBAction)SaveButtonTouched:(id)sender;

@end
