//
//  SBLocationTableViewController.m
//  SBMapApp
//
//  Created by Student on 2015-03-25.
//  Copyright (c) 2015 Student. All rights reserved.
//

#import "SBLocationTableViewController.h"
#import "SBLocationModel.h"
#import "SBGMapsViewController.h"

@interface SBLocationTableViewController ()

@end

@implementation SBLocationTableViewController
{
    NSMutableData* _responseData;
    NSMutableDictionary* table;
    NSMutableArray* table_data;
    SBGMapsViewController* map_view;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return table.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSArray* allkeys = [table allKeys]; //table_data.count;
    NSString* key = [allkeys objectAtIndex:section];
    NSArray* sction = [table objectForKey:key];
    return sction.count;
}

// Send an http request for the json file on the server
- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    // Create the request.
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://sbennett.imgd.ca/locations.json"]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (UITableViewCell *)tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Configure the cell...
    static NSString* CellIdentifier = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:
                             CellIdentifier forIndexPath:indexPath];
    
    NSArray* all_sections = [table allKeys];
    NSString* main_section = [all_sections objectAtIndex: indexPath.section];
    
    NSDictionary* province = [table objectForKey: main_section];
    
    NSArray* cities = [province allKeys];
    NSString* which_city = [cities objectAtIndex: indexPath.row];
    
    cell.textLabel.text = which_city;
    
    SBLocationModel* location = [[SBLocationModel alloc] initFromTable:[province objectForKey: which_city]];
    
    cell.detailTextLabel.text = [NSString stringWithFormat: @"(%f, %f)", location.x, location.y];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray* all_keys = [table allKeys];
    return [all_keys objectAtIndex: section];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender :(id)sender {
    
    NSIndexPath* indexPath = [self.tableView indexPathForCell:sender];
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    map_view = (SBGMapsViewController*)segue.destinationViewController;
    
    NSArray* all_sections = [table allKeys];
    NSString* main_section = [all_sections objectAtIndex: indexPath.section];
    
    NSDictionary* province = [table objectForKey: main_section];
    
    NSArray* cities = [province allKeys];
    NSString* which_city = [cities objectAtIndex: indexPath.row];
    
    NSLog(@"You selected %@", which_city);
    
    NSLog(@"%@", table);
    
    SBLocationModel* location = [[SBLocationModel alloc] initFromTable:[province objectForKey: which_city]];
    location.name = which_city;
    
    NSLog(@"Locations name: %@", location.name);
    
    map_view.selected_location = location;
    map_view.transition = YES;
}


#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    NSLog(@"Finished loading data from sbennett.imgd.ca");
    NSError* error = [NSError errorWithDomain:@"http json request" code:-1 userInfo:nil];
    
    id object = [NSJSONSerialization JSONObjectWithData:_responseData options:NSJSONReadingAllowFragments error:&error];
    
    if ([object isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"\n%@", object);
    }
    else
        NSLog(@"json data not as expected");
    
    table = [NSMutableDictionary new];
    
    [object enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        
        // We expect the type of this object to be a table
        if([obj isKindOfClass:[NSArray class]])
        {
            NSMutableArray* new_array = [NSMutableArray array];
            
            [table setObject: new_array forKey: key];
            
            [obj enumerateKeysAndObjectsUsingBlock:^(id key2, id obj2, BOOL *stop) {
                
                // We expect the type of this object to be an array
                if([obj2 isKindOfClass:[NSArray class]])
                {
                    SBLocationModel* new_model = [[SBLocationModel alloc] initFromTable:obj];
                    new_model.name = key2;
                    
                    [new_array addObject: new_model];
                                      
                    new_model.index = [new_array indexOfObject: new_model];
                }
                
            }];
        }
        [table setObject: obj forKey: key];
    }];
    
    [self.tableView reloadData];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

@end
